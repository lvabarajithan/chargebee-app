package com.chargebee;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AppTest {

    @Rule
    public ActivityTestRule<EntryActivity> activityTestRule = new ActivityTestRule<>(EntryActivity.class);

    @Test
    public void allPagesTest() {

        onView(withId(R.id.entry_proceed_btn)).perform(click());
        onView(withId(R.id.main_add_fab)).perform(click());

    }
}
