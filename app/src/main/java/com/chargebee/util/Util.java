package com.chargebee.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by abara on 05/09/17.
 */

public class Util {

    public static final String PREF_SITE_NAME = "site_name";
    public static final String PREF_AUTH_TOKEN = "auth_token";
    public static final String PREF_DETAILS_PROVIDED = "details_provided";

    public static String parseDate(long timestamp) {
        Date date = new Date(timestamp);
        return new SimpleDateFormat("dd MMM yyyy", Locale.US).format(date);
    }

    public static Spanned colourize(String prefix, String value) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(prefix + "<b><font color='#7864ff'>" + value + "</font></b>", Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(prefix + "<b><font color='#7864ff'>" + value + "</font></b>");
        }

    }

    /*public static void checkConnection(Context context, final ConnectedState state) {
        if (isNetworkAvailable(context)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://clients3.google.com/generate_204").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        state.onConnectionState((urlc.getResponseCode() == 200 && urlc.getContentLength() == 0));
                    } catch (IOException e) {
                        e.printStackTrace();
                        state.onConnectionState(false);
                    }
                }
            }).start();
        } else {
            state.onConnectionState(false);
        }
    }*/

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
