package com.chargebee.api;

import android.support.annotation.Nullable;

import com.chargebee.model.Result;
import com.chargebee.model.ResultEntity;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abara on 03/09/17.
 */

public class ChargeBeeClient {

    public static final int DEFAULT_LIMIT = 10;

    private static final String BASE_URL_FRONT = "https://";
    private static final String BASE_URL = ".chargebee.com/api/v2/";

    private static Retrofit.Builder retrofitBuilder;
    private static Retrofit retrofit;

    private static String siteName;
    private static String authToken;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static ChargeBeeService chargeBeeService;

    public static void init(String siteName, String authToken) {
        if (!siteName.isEmpty() && !authToken.isEmpty()) {
            ChargeBeeClient.siteName = siteName;
            ChargeBeeClient.authToken = authToken;
        }
        retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BASE_URL_FRONT + ChargeBeeClient.siteName + BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        chargeBeeService = createService();
    }

    public static void listSubscriptions(String offset, final OnSubscriptionResponse subscriptionResponse) {

        chargeBeeService.listSubscriptions(DEFAULT_LIMIT, offset).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@Nullable Call<Result> call, @Nullable Response<Result> response) {

                if (response != null) {
                    Result result = response.body();
                    if (result != null) {
                        subscriptionResponse.onResult(result, result.getNextOffset());
                    } else {
                        subscriptionResponse.onFailure("Something is wrong!");
                    }
                } else {
                    subscriptionResponse.onFailure("ChargeBee Server error!");
                }

            }

            @Override
            public void onFailure(@Nullable Call<Result> call, @Nullable Throwable t) {
                String error = "Failure!";
                if (t != null) {
                    error = t.getMessage();
                }
                subscriptionResponse.onFailure(error);
            }
        });

    }

    public static void createSubscription(String planId, String firstName, String lastName, String email, final OnResponse createResponse) {

        chargeBeeService.createSubscriptions(planId, firstName, lastName, email).enqueue(new Callback<ResultEntity>() {
            @Override
            public void onResponse(@Nullable Call<ResultEntity> call, @Nullable Response<ResultEntity> response) {
                if (response != null) {
                    ResultEntity entityResponse = response.body();
                    if (entityResponse != null) {
                        createResponse.onResponse(entityResponse);
                    } else {
                        createResponse.onFailure("Subscription not created!");
                    }
                } else {
                    createResponse.onFailure("Subscription not created!");
                }
            }

            @Override
            public void onFailure(@Nullable Call<ResultEntity> call, @Nullable Throwable t) {
                String error = "Failure!";
                if (t != null) {
                    error = t.getMessage();
                }
                createResponse.onFailure(error);
            }
        });

    }

    public static void deleteSubscription(String id, final OnResponse deleteResponse) {

        chargeBeeService.deleteSubscription(id).enqueue(new Callback<ResultEntity>() {
            @Override
            public void onResponse(@Nullable Call<ResultEntity> call, @Nullable Response<ResultEntity> response) {
                if (response != null) {
                    ResultEntity entityResponse = response.body();
                    if (entityResponse != null) {
                        deleteResponse.onResponse(entityResponse);
                    } else {
                        deleteResponse.onFailure("Subscription not deleted!");
                    }
                } else {
                    deleteResponse.onFailure("Subscription not deleted!");
                }
            }

            @Override
            public void onFailure(@Nullable Call<ResultEntity> call, @Nullable Throwable t) {
                String error = "Failure!";
                if (t != null) {
                    error = t.getMessage();
                }
                deleteResponse.onFailure(error);
            }
        });

    }

    private static ChargeBeeService createService() {
        BasicAuthInterceptor authInterceptor = new BasicAuthInterceptor(authToken);
        if (!httpClient.interceptors().contains(authInterceptor)) {
            httpClient.addInterceptor(authInterceptor);
            retrofitBuilder.client(httpClient.build());
            retrofit = retrofitBuilder.build();
        }
        return retrofit.create(ChargeBeeService.class);
    }
}
