package com.chargebee.api;

import com.chargebee.model.ResultEntity;

/**
 * Created by abara on 07/09/17.
 */

public interface OnResponse {
    void onResponse(ResultEntity resultEntity);

    void onFailure(String error);
}
