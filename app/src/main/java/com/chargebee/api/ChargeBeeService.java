package com.chargebee.api;

import com.chargebee.model.Result;
import com.chargebee.model.ResultEntity;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by abara on 03/09/17.
 */

public interface ChargeBeeService {
    @GET("subscriptions")
    Call<Result> listSubscriptions(@Query("limit") int limit, @Query("offset") String offset);

    @POST("subscriptions")
    Call<ResultEntity> createSubscriptions(
            @Query("plan_id") String planId,
            @Query("customer[first_name]") String firstName,
            @Query("customer[last_name]") String lastName,
            @Query("customer[email]") String email
    );

    @POST("subscriptions/{id}/delete")
    Call<ResultEntity> deleteSubscription(@Path("id") String subscriptionId);

}
