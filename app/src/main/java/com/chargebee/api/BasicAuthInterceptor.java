package com.chargebee.api;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by abara on 03/09/17.
 */

public class BasicAuthInterceptor implements Interceptor {

    private String token;

    public BasicAuthInterceptor(String username) {
        this.token = Credentials.basic(username, "");
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Request authReq = request.newBuilder()
                .header("Authorization", token)
                .build();
        return chain.proceed(authReq);
    }

}
