package com.chargebee.api;

import android.support.annotation.Nullable;

import com.chargebee.model.Result;

/**
 * Created by abara on 03/09/17.
 */

public interface OnSubscriptionResponse {

    void onResult(Result result, @Nullable String nextOffset);

    void onFailure(String error);

}
