package com.chargebee;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chargebee.adapter.OnItemClickListener;
import com.chargebee.adapter.SubscriptionAdapter;
import com.chargebee.api.ChargeBeeClient;
import com.chargebee.api.OnResponse;
import com.chargebee.api.OnSubscriptionResponse;
import com.chargebee.db.CacheORM;
import com.chargebee.model.Result;
import com.chargebee.model.ResultEntity;
import com.chargebee.util.Util;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, OnSubscriptionResponse, OnItemClickListener, OnResponse {

    private static final int REQUEST_CODE_CREATE = 99;

    private SharedPreferences prefs;

    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout loadingLayout;
    private FloatingActionButton addFab;
    private RecyclerView mainList;

    private SubscriptionAdapter adapter;
    private CacheLoaderTask cacheLoaderTask;

    private String nextOffset = "";
    private boolean isFetching = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set Toolbar
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String site = prefs.getString(Util.PREF_SITE_NAME, "klnce-test");
        String token = prefs.getString(Util.PREF_AUTH_TOKEN, "test_OE7Ae0XdhXAQfZsALTw0Jcu108ZwPG7sz");
        ChargeBeeClient.init(site, token);

        // Init views.
        loadingLayout = findViewById(R.id.main_loading_layout);
        swipeRefreshLayout = findViewById(R.id.main_swipe_to_refresh);
        addFab = findViewById(R.id.main_add_fab);
        addFab.setOnClickListener(this);

        // Init RecyclerView
        mainList = findViewById(R.id.main_list);
        mainList.setLayoutManager(new LinearLayoutManager(this));
        mainList.setItemAnimator(new DefaultItemAnimator());
        mainList.setHasFixedSize(false);
        mainList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(RecyclerView.VERTICAL)) {
                    // The List cannot be scrolled further. So Load items.
                    if (Util.isNetworkAvailable(MainActivity.this))
                        sendRequestWithOffset();
                } else if (dy < 0) {
                    onScrolledUp();
                } else if (dy > 0) {
                    onScrolledDown();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        // Init SubscriptionAdapter.
        adapter = new SubscriptionAdapter(this);
        mainList.setAdapter(adapter);
        if (Util.isNetworkAvailable(this)) {
            CacheORM.clearCache(this);
            sendRequestWithOffset();
        } else {
            cacheLoaderTask = new CacheLoaderTask();
            cacheLoaderTask.execute();
            Toast.makeText(this, "No Internet connection!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cacheLoaderTask != null)
            cacheLoaderTask.cancel(true);
    }

    private void onScrolledDown() {
        addFab.hide();
    }

    private void onScrolledUp() {
        addFab.show();
    }

    private void sendRequestWithOffset() {
        if (nextOffset != null) {
            ChargeBeeClient.listSubscriptions(nextOffset, this);
            addFab.hide();
            isFetching = true;
            swipeRefreshLayout.setRefreshing(true);
        } else {
            hideSwipeToRefresh();
        }
    }

    private void populateList(final ArrayList<ResultEntity> entities, boolean shouldCache) {

        adapter.appendData(entities, nextOffset == null);
        adapter.notifyDataSetChanged();

        if (shouldCache)
            CacheORM.cacheResult(this, new Result(entities));

    }

    private void hideSwipeToRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
            isFetching = false;
        }
    }

    @Override
    public void onRefresh() {
        if (Util.isNetworkAvailable(this) && !isFetching) {
            refreshList();
            return;
        }
        hideSwipeToRefresh();
    }

    private void refreshList() {
        // Clear cache
        CacheORM.clearCache(this);

        // clear adapter data
        adapter.clearData();

        // Set empty offset
        nextOffset = "";

        // Send a request
        sendRequestWithOffset();
    }

    @Override
    public void onClick(View view) {
        startActivityForResult(new Intent(this, CreateSubscriptionActivity.class), REQUEST_CODE_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CREATE && resultCode == RESULT_OK) {
            refreshList();
        }
    }

    @Override
    public void onResult(Result result, @Nullable String nextOffset) {

        this.nextOffset = nextOffset;

        populateList(result.getList(), true);

        loadingLayout.setVisibility(View.GONE);

        hideSwipeToRefresh();
        addFab.show();
        isFetching = false;

    }

    @Override
    public void onResponse(ResultEntity resultEntity) {
        String name = resultEntity.getCustomer().getName();
        Toast.makeText(this, "Subscription deleted: " + name, Toast.LENGTH_SHORT).show();
        refreshList();
    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(ResultEntity entity) {
        Intent detailsIntent = new Intent(this, DetailsActivity.class);
        detailsIntent.putExtra("entity", entity);
        startActivity(detailsIntent);
    }

    @Override
    public void onItemLongClick(final String id) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_subscription_delete_title)
                .setPositiveButton(R.string.dialog_delete_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ChargeBeeClient.deleteSubscription(id, MainActivity.this);
                    }
                })
                .setNegativeButton(R.string.dialog_delete_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_change_site:
                resetApp();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void resetApp() {

        prefs.edit().putBoolean(Util.PREF_DETAILS_PROVIDED, false).apply();

        CacheORM.clearCache(this);

        startActivity(new Intent(this, EntryActivity.class));
        finish();

    }

    private class CacheLoaderTask extends AsyncTask<Void, Void, Result> {

        @Override
        protected Result doInBackground(Void... voids) {
            return CacheORM.loadFromCache(MainActivity.this);
        }

        @Override
        protected void onPostExecute(Result result) {
            populateList(result.getList(), false);
            nextOffset = "";

            loadingLayout.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Log.d("ABBDB", "onCancelled: Cancelled!");
        }
    }

}
