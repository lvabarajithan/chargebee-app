package com.chargebee.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by abara on 09/09/17.
 */

public class CacheWrapper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cache_list.db";
    private static final int DATABASE_VERSION = 1;

    CacheWrapper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CacheORM.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(CacheORM.DROP_TABLE);
        onCreate(db);
    }
}
