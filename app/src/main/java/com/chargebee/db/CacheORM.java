package com.chargebee.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.chargebee.model.Customer;
import com.chargebee.model.Result;
import com.chargebee.model.ResultEntity;
import com.chargebee.model.Subscription;

import java.util.ArrayList;

/**
 * Created by abara on 09/09/17.
 */

public class CacheORM {

    // Subscription
    private static final String COL_SUB_ID_TYPE = "TEXT";
    private static final String COL_SUB_ID = "sub_id";
    private static final String COL_STATUS_TYPE = "TEXT";
    private static final String COL_STATUS = "status";
    private static final String COL_CREATED_AT_TYPE = "LONG";
    private static final String COL_CREATED_AT = "created_at";
    private static final String COL_NEXT_BILLING_TYPE = "LONG";
    private static final String COL_NEXT_BILLING = "next_billing";
    private static final String COL_PLAN_ID_TYPE = "TEXT";
    private static final String COL_PLAN_ID = "plan_id";
    private static final String COL_PLAN_QUANTITY_TYPE = "INTEGER";
    private static final String COL_PLAN_QUANTITY = "plan_quantity";
    private static final String COL_CURR_CODE_TYPE = "TEXT";
    private static final String COL_CURR_CODE = "curr_code";
    private static final String COL_BILLING_PERIOD_TYPE = "INTEGER";
    private static final String COL_BILLING_PERIOD = "billing_period";
    private static final String COL_BILLING_UNIT_TYPE = "TEXT";
    private static final String COL_BILLING_UNIT = "billing_unit";
    // Customer
    private static final String COL_CUST_ID_TYPE = "TEXT";
    private static final String COL_CUST_ID = "cust_id";
    private static final String COL_FNAME_TYPE = "TEXT";
    private static final String COL_FNAME = "fname";
    private static final String COL_LNAME_TYPE = "TEXT";
    private static final String COL_LNAME = "lname";
    private static final String COL_EMAIL_TYPE = "TEXT";
    private static final String COL_EMAIL = "email";
    private static final String TABLE_NAME = "cache";
    static final String DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SEP = ", ";
    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COL_SUB_ID + " " + COL_SUB_ID_TYPE + SEP +
                    COL_STATUS + " " + COL_STATUS_TYPE + SEP +
                    COL_CREATED_AT + " " + COL_CREATED_AT_TYPE + SEP +
                    COL_NEXT_BILLING + " " + COL_NEXT_BILLING_TYPE + SEP +
                    COL_PLAN_ID + " " + COL_PLAN_ID_TYPE + SEP +
                    COL_PLAN_QUANTITY + " " + COL_PLAN_QUANTITY_TYPE + SEP +
                    COL_CURR_CODE + " " + COL_CURR_CODE_TYPE + SEP +
                    COL_BILLING_PERIOD + " " + COL_BILLING_PERIOD_TYPE + SEP +
                    COL_BILLING_UNIT + " " + COL_BILLING_UNIT_TYPE + SEP +
                    COL_CUST_ID + " " + COL_CUST_ID_TYPE + SEP +
                    COL_FNAME + " " + COL_FNAME_TYPE + SEP +
                    COL_LNAME + " " + COL_LNAME_TYPE + SEP +
                    COL_EMAIL + " " + COL_EMAIL_TYPE +
                    ")";

    public static void cacheResult(Context context, Result result) {

        CacheWrapper cacheWrapper = new CacheWrapper(context);
        SQLiteDatabase db = cacheWrapper.getWritableDatabase();

        int count = 0;
        for (ResultEntity entity : result.getList()) {
            ContentValues values = getValues(entity);

            db.insert(TABLE_NAME, null, values);
            count++;

        }
        db.close();
        Log.d("ABBDB", "cacheResult: Cached -> " + count);

    }

    private static ContentValues getValues(ResultEntity entity) {

        ContentValues values = new ContentValues();

        Subscription s = entity.getSubscription();
        values.put(COL_SUB_ID, s.getId());
        values.put(COL_STATUS, s.getStatus());
        values.put(COL_CREATED_AT, s.getCreatedAt());
        values.put(COL_NEXT_BILLING, s.getNextBllingAt());
        values.put(COL_PLAN_ID, s.getPlanId());
        values.put(COL_PLAN_QUANTITY, s.getPlanQuantity());
        values.put(COL_CURR_CODE, s.getCurrencyCode());
        values.put(COL_BILLING_PERIOD, s.getBillingPeriod());
        values.put(COL_BILLING_UNIT, s.getBillingPeriodUnit());

        Customer c = entity.getCustomer();
        values.put(COL_CUST_ID, c.getId());
        values.put(COL_FNAME, c.getFirstName());
        values.put(COL_LNAME, c.getLastName());
        values.put(COL_EMAIL, c.getEmail());

        return values;
    }

    public static Result loadFromCache(Context context) {

        CacheWrapper cacheWrapper = new CacheWrapper(context);
        SQLiteDatabase db = cacheWrapper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        ArrayList<ResultEntity> list = new ArrayList<>();

        if (cursor.getCount() > 0) {

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ResultEntity entity = getEntity(cursor);
                list.add(entity);
                cursor.moveToNext();
            }

        }
        cursor.close();
        db.close();

        Log.d("ABBDB", "loadFromCache: Data loaded from cache");

        return new Result(list);

    }

    private static ResultEntity getEntity(Cursor cursor) {

        Subscription s = new Subscription();
        s.setId(cursor.getString(cursor.getColumnIndex(COL_SUB_ID)));
        s.setStatus(cursor.getString(cursor.getColumnIndex(COL_STATUS)));
        s.setCreatedAt(cursor.getLong(cursor.getColumnIndex(COL_CREATED_AT)));
        s.setNextBillingAt(cursor.getLong(cursor.getColumnIndex(COL_NEXT_BILLING)));
        s.setPlanId(cursor.getString(cursor.getColumnIndex(COL_PLAN_ID)));
        s.setPlanQuantity(cursor.getInt(cursor.getColumnIndex(COL_PLAN_QUANTITY)));
        s.setCurrencyCode(cursor.getString(cursor.getColumnIndex(COL_CURR_CODE)));
        s.setBillingPeriod(cursor.getInt(cursor.getColumnIndex(COL_BILLING_PERIOD)));
        s.setBillingPeriodUnit(cursor.getString(cursor.getColumnIndex(COL_BILLING_UNIT)));

        Customer c = new Customer();
        c.setId(cursor.getString(cursor.getColumnIndex(COL_CUST_ID)));
        c.setFirstName(cursor.getString(cursor.getColumnIndex(COL_FNAME)));
        c.setLastName(cursor.getString(cursor.getColumnIndex(COL_LNAME)));
        c.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));

        return new ResultEntity(s, c);
    }

    public static void clearCache(Context context) {

        CacheWrapper cacheWrapper = new CacheWrapper(context);
        SQLiteDatabase db = cacheWrapper.getReadableDatabase();

        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.close();
        Log.d("ABBDB", "clearCache: Cache cleared!");

    }

}
