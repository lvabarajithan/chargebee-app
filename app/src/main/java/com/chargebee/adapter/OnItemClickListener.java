package com.chargebee.adapter;

import com.chargebee.model.ResultEntity;

/**
 * Created by abara on 08/09/17.
 */

public interface OnItemClickListener {
    void onItemClick(ResultEntity entity);

    void onItemLongClick(String id);
}
