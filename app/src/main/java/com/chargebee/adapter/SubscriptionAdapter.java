package com.chargebee.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.chargebee.R;
import com.chargebee.model.Customer;
import com.chargebee.model.ResultEntity;
import com.chargebee.model.Subscription;
import com.chargebee.util.Util;

import java.util.ArrayList;

import static com.chargebee.util.Util.colourize;

/**
 * Created by abara on 04/09/17.
 */

public class SubscriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_LOADER = 1;

    private ArrayList<ResultEntity> entities = new ArrayList<>();

    private int totalSize;
    private int loaderPosition;
    private OnItemClickListener clickListener;

    public SubscriptionAdapter(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void appendData(ArrayList<ResultEntity> newEntities, boolean hasListLoaded) {
        entities.addAll(newEntities);
        totalSize = hasListLoaded ? entities.size() : (entities.size() + TYPE_LOADER);
        loaderPosition = hasListLoaded ? totalSize + 1 : totalSize - 1;
    }

    public void clearData() {
        entities.clear();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_LOADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_loading, parent, false);
            return new LoaderHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_subscription, parent, false);
            return new SubscriptionHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder.getAdapterPosition() != loaderPosition) {
            SubscriptionHolder subscriptionHolder = (SubscriptionHolder) holder;

            Customer customer = entities.get(holder.getAdapterPosition()).getCustomer();
            subscriptionHolder.customerName.setText(customer.getName());
            subscriptionHolder.customerEmail.setText(customer.getEmail());

            Subscription subscription = entities.get(holder.getAdapterPosition()).getSubscription();
            subscriptionHolder.subscriptionId.setText(colourize("Id: ", subscription.getId()));
            subscriptionHolder.status.setText(colourize("Status: ", subscription.getStatus()));
            subscriptionHolder.createdAt.setText(colourize("Created at: ", Util.parseDate(subscription.getCreatedAt())));
            subscriptionHolder.nextBillingAt.setText(colourize("Next billing at: ", Util.parseDate(subscription.getNextBllingAt())));
            subscriptionHolder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(entities.get(holder.getAdapterPosition()));
                }
            });
            subscriptionHolder.layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    clickListener.onItemLongClick(entities.get(holder.getAdapterPosition()).getSubscription().getId());
                    return true;
                }
            });
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == loaderPosition) {
            // Last item is always loader
            return TYPE_LOADER;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        Log.d("ABB", "Count Size " + totalSize);
        return totalSize;
    }

    class SubscriptionHolder extends RecyclerView.ViewHolder {

        AppCompatTextView customerName;
        AppCompatTextView customerEmail;
        AppCompatTextView status;
        AppCompatTextView subscriptionId;
        AppCompatTextView createdAt;
        AppCompatTextView nextBillingAt;
        ConstraintLayout layout;

        SubscriptionHolder(View itemView) {
            super(itemView);

            customerName = itemView.findViewById(R.id.item_sub_name);
            customerEmail = itemView.findViewById(R.id.item_sub_email);
            subscriptionId = itemView.findViewById(R.id.item_sub_id);
            status = itemView.findViewById(R.id.item_sub_status);
            createdAt = itemView.findViewById(R.id.item_sub_created_at);
            nextBillingAt = itemView.findViewById(R.id.item_sub_next_billing);

            layout = itemView.findViewById(R.id.item_sub);

        }
    }

    class LoaderHolder extends RecyclerView.ViewHolder {

        ProgressBar loader;

        LoaderHolder(View itemView) {
            super(itemView);

            loader = itemView.findViewById(R.id.single_item_loader);

        }
    }

}
