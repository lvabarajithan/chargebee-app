package com.chargebee.model;

import java.util.ArrayList;

/**
 * Created by abara on 03/09/17.
 */

public class Result {

    private ArrayList<ResultEntity> list;
    private String next_offset;

    public Result(ArrayList<ResultEntity> entities) {
        this.list = entities;
        this.next_offset = "";
    }

    public Result() {
    }

    public ArrayList<ResultEntity> getList() {
        return list;
    }

    public String getNextOffset() {
        return next_offset;
    }
}
