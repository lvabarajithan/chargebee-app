package com.chargebee.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abara on 03/09/17.
 */

public class ResultEntity implements Parcelable {

    public static final Creator<ResultEntity> CREATOR = new Creator<ResultEntity>() {
        @Override
        public ResultEntity createFromParcel(Parcel in) {
            return new ResultEntity(in);
        }

        @Override
        public ResultEntity[] newArray(int size) {
            return new ResultEntity[size];
        }
    };
    private Subscription subscription;
    private Customer customer;

    public ResultEntity() {
    }

    public ResultEntity(Subscription subscription, Customer customer) {
        this.subscription = subscription;
        this.customer = customer;
    }

    private ResultEntity(Parcel in) {
        subscription = in.readParcelable(Subscription.class.getClassLoader());
        customer = in.readParcelable(Customer.class.getClassLoader());
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(subscription, i);
        parcel.writeParcelable(customer, i);
    }
}
