package com.chargebee.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abara on 03/09/17.
 */

public class Subscription implements Parcelable {

    public static final Creator<Subscription> CREATOR = new Creator<Subscription>() {
        @Override
        public Subscription createFromParcel(Parcel in) {
            return new Subscription(in);
        }

        @Override
        public Subscription[] newArray(int size) {
            return new Subscription[size];
        }
    };
    private String id;
    private String status;
    private long created_at;
    private long next_billing_at;
    private String plan_id;
    private int plan_quantity;
    private String currency_code;
    private int billing_period;
    private String billing_period_unit;

    public Subscription() {
    }

    private Subscription(Parcel in) {
        id = in.readString();
        status = in.readString();
        created_at = in.readLong();
        next_billing_at = in.readLong();
        plan_id = in.readString();
        plan_quantity = in.readInt();
        currency_code = in.readString();
        billing_period = in.readInt();
        billing_period_unit = in.readString();
    }

    public void setNextBillingAt(long next_billing_at) {
        this.next_billing_at = next_billing_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(long created_at) {
        this.created_at = created_at;
    }

    public long getNextBllingAt() {
        return next_billing_at;
    }

    public String getPlanId() {
        return plan_id;
    }

    public void setPlanId(String plan_id) {
        this.plan_id = plan_id;
    }

    public int getPlanQuantity() {
        return plan_quantity;
    }

    public void setPlanQuantity(int plan_quantity) {
        this.plan_quantity = plan_quantity;
    }

    public String getCurrencyCode() {
        return currency_code;
    }

    public void setCurrencyCode(String currency_code) {
        this.currency_code = currency_code;
    }

    public int getBillingPeriod() {
        return billing_period;
    }

    public void setBillingPeriod(int billing_period) {
        this.billing_period = billing_period;
    }

    public String getBillingPeriodUnit() {
        return billing_period_unit;
    }

    public void setBillingPeriodUnit(String billing_period_unit) {
        this.billing_period_unit = billing_period_unit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(status);
        parcel.writeLong(created_at);
        parcel.writeLong(next_billing_at);
        parcel.writeString(plan_id);
        parcel.writeInt(plan_quantity);
        parcel.writeString(currency_code);
        parcel.writeInt(billing_period);
        parcel.writeString(billing_period_unit);
    }

}
