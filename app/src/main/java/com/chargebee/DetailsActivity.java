package com.chargebee;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.chargebee.model.Customer;
import com.chargebee.model.ResultEntity;
import com.chargebee.model.Subscription;

import static com.chargebee.util.Util.colourize;

/**
 * Created by abara on 08/09/17.
 */

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ResultEntity entity = getIntent().getParcelableExtra("entity");

        Customer customer = entity.getCustomer();
        Subscription subscription = entity.getSubscription();

        AppCompatTextView nameText = findViewById(R.id.details_name);
        nameText.setText(customer.getName());

        AppCompatTextView emailText = findViewById(R.id.details_email);
        emailText.setText(customer.getEmail());

        AppCompatTextView subId = findViewById(R.id.details_sub_id);
        subId.setText(colourize("Subscription Id: ", subscription.getId()));

        AppCompatTextView planId = findViewById(R.id.details_plan_id);
        planId.setText(colourize("Plan Id: ", subscription.getPlanId()));

        AppCompatTextView planQuantity = findViewById(R.id.details_plan_quantity);
        planQuantity.setText(colourize("Plan Quantity: ", subscription.getPlanQuantity() + ""));

        AppCompatTextView status = findViewById(R.id.details_status);
        status.setText(colourize("Status: ", subscription.getStatus()));

        AppCompatTextView currCode = findViewById(R.id.details_curr_code);
        currCode.setText(colourize("Currency code: ", subscription.getCurrencyCode()));

        AppCompatTextView billPeriod = findViewById(R.id.details_bill_period);
        billPeriod.setText(colourize("Billing period: ", "" + subscription.getBillingPeriod()));

        AppCompatTextView billUnit = findViewById(R.id.details_bill_uni);
        billUnit.setText(colourize("Billing unit: ", subscription.getBillingPeriodUnit()));

        findViewById(R.id.details_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
