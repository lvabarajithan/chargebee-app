package com.chargebee;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.chargebee.api.ChargeBeeClient;
import com.chargebee.api.OnResponse;
import com.chargebee.model.ResultEntity;
import com.chargebee.util.Util;

/**
 * Created by abara on 06/09/17.
 */

public class CreateSubscriptionActivity extends AppCompatActivity implements View.OnClickListener, OnResponse {

    private TextInputEditText fnameBox;
    private TextInputEditText lnameBox;
    private TextInputEditText emailBox;
    private TextInputEditText planId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_subscription);

        fnameBox = findViewById(R.id.create_sub_fname);
        lnameBox = findViewById(R.id.create_sub_lname);
        emailBox = findViewById(R.id.create_sub_email);
        planId = findViewById(R.id.create_sub_planid);

        findViewById(R.id.create_cancel_btn).setOnClickListener(this);
        findViewById(R.id.create_sub_btn).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.create_cancel_btn:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.create_sub_btn:
                if (Util.isNetworkAvailable(this))
                    createSubscription();
                else
                    Toast.makeText(this, "No Internet connection!", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void createSubscription() {

        String firstName = fnameBox.getText().toString();
        String lastName = lnameBox.getText().toString();
        String email = emailBox.getText().toString();
        String planid = planId.getText().toString();

        if (!firstName.isEmpty() && !lastName.isEmpty() && !email.isEmpty()) {

            ChargeBeeClient.createSubscription(planid, firstName, lastName, email, this);

        } else {
            Toast.makeText(this, "All field(s) required!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResponse(ResultEntity resultEntity) {
        String name = resultEntity.getCustomer().getName();
        Toast.makeText(this, "Subscription created for " + name, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
