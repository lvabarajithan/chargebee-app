package com.chargebee;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Toast;

import com.chargebee.util.Util;

/**
 * Created by abara on 03/09/17.
 */

public class EntryActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText siteBox;
    private TextInputEditText tokenBox;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        siteBox = findViewById(R.id.entry_site_box);
        tokenBox = findViewById(R.id.entry_token_box);
        AppCompatButton proceedBtn = findViewById(R.id.entry_proceed_btn);

        proceedBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (Util.isNetworkAvailable(this)) {

            String site = siteBox.getText().toString();
            String token = tokenBox.getText().toString();

            if (site.isEmpty()) site = "klnce-test";
            if (token.isEmpty()) token = "test_OE7Ae0XdhXAQfZsALTw0Jcu108ZwPG7sz";

            prefs.edit().putString(Util.PREF_SITE_NAME, site)
                    .putString(Util.PREF_AUTH_TOKEN, token)
                    .putBoolean(Util.PREF_DETAILS_PROVIDED, true)
                    .apply();

            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Toast.makeText(this, "No Internet connection!", Toast.LENGTH_SHORT).show();
        }

    }
}
